import { MentorshipWebPage } from './app.po';

describe('mentorship-web App', function() {
  let page: MentorshipWebPage;

  beforeEach(() => {
    page = new MentorshipWebPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
