import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppWebRoutingModule }  from './configuration/app-routing.module';

import { TableComponent } from './components/table-component/table.component';

import { AppComponent } from './app.component';


@NgModule({
  declarations: [
    AppComponent,
    TableComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppWebRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
