import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
declare var require: any;

//---------------------------- Components -------------------------------------
import { TableComponent } from './../components/table-component/table.component';

export const routes: Routes = [
  { path: 'table', component: TableComponent}
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppWebRoutingModule { }
